# Configuração

Adicione no PATH e/ou crie alias dos comandos.

Exemplo:

Adicione no .bashrc ou .zshrc

    export SCRIPTS_UTIL="$HOME/scripts-util"
    export PATH="$PATH:$SCRIPTS_UTIL/kubernetes/"


Para utilizar só executar:

    $ kube-logs app-pod app-namespace # nome do pod e namespace