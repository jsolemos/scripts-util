#!/bin/bash

echo "Para configurar o git"
echo "Qual o seu nome?"
read MY_NAME

echo "Qual seu email?"
read MY_EMAIL

echo "Iniciando a instalação de pricipais ferramentas"
echo "Dados da distribuição"
cat /etc/lsb-release


# UPDATE
sudo apt-get update -y 
sudo apt-get install -y git curl zsh build-essential

# git
git config --global user.email $MY_EMAIL
git config --global user.name $MY_NAME

# SSH 
echo "Gerar sua chave ssh"
ssh-keygen -o -t rsa -C $MY_EMAIL -b 4096

# ZSH / oh-my-zsh
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
sudo chsh -s /bin/zsh $USER
