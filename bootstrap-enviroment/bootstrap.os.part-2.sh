
sudo apt-get install -y nmap virtualbox

# NVM
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash

# cat <<EOT >> ~/.zshrc

# export NVM_DIR="${XDG_CONFIG_HOME/:-$HOME/.}nvm"
# [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
# EOT

# source  ~/.zshrc

# node
nvm install 12
# Modules
npm i -g httpserver nodemon typescript ts-node node-gyp

# Flatpak install 
# sudo apt install -y flatpak
# sudo flatpak install flathub com.visualstudio.code
# sudo flatpak install flathub io.dbeaver.DBeaverCommunity
# sudo flatpak install flathub com.slack.Slack

# Snap install 
# sudo snap install code --classic
# sudo snap install insomnia

# GCLOUD
# wget https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-240.0.0-linux-x86_64.tar.gz
# tar zxvf google-cloud-sdk*.tar.gz google-cloud-sdk
# ./google-cloud-sdk/install.sh
echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] http://packages.cloud.google.com/apt cloud-sdk main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list

# Import the Google Cloud Platform public key
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -

# Update the package list and install the Cloud SDK
sudo apt update && sudo apt install google-cloud-sdk -y

# # CHROME 
# wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add - 
# sudo sh -c 'echo "deb https://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
# sudo apt-get update
# sudo apt-get install google-chrome-stable


# DOCKER
sudo apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common -y
curl -fsSL https://get.docker.com | sh
sudo usermod -aG docker $USER
sudo curl -L "https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# AWS - ECS-CLI 
sudo curl -o /usr/local/bin/ecs-cli https://s3.amazonaws.com/amazon-ecs-cli/ecs-cli-linux-amd64-latest
sudo chmod +x /usr/local/bin/ecs-cli
sudo curl -o /usr/local/bin/aws-iam-authenticator https://amazon-eks.s3-us-west-2.amazonaws.com/1.15.10/2020-02-22/bin/linux/amd64/aws-iam-authenticator
sudo chmod +x /usr/local/bin/aws-iam-authenticator

#
git clone git@gitlab.com:jsolemos/scripts-util.git

# Variaveis de ambiente
cat <<EOT >> ~/.zshrc
export MPLBACKEND=TkAgg
export REACT_EDITOR=xed
export SCRIPTS_UTIL="\$HOME/scripts-util"
export PATH="\$PATH:\$SCRIPTS_UTIL/kubernetes:\$SCRIPTS_UTIL/docker:\$SCRIPTS_UTIL/vpn"

EOT

# ANDROID, chrome, others
sudo add-apt-repository ppa:maarten-fonville/android-studio -y
sudo apt install google-chrome-stable dbeaver-ce android-sdk android-sdk-build-tools android-studio -f

#KVM
sudo apt-get install qemu-kvm libvirt-daemon-system libvirt-clients bridge-utils -y
sudo adduser `id -un` libvirt
sudo adduser `id -un` kvm


# JAVA
# sudo add-apt-repository ppa:webupd8team/java -y
# sudo apt update -y
# sudo apt install oracle-java8-installer -y

# Variaveis de ambiente
cat <<EOT >> ~/.zshrc

export ANDROID_HOME="\$HOME/Android/Sdk"
export JAVA_HOME="/usr/lib/jvm/java-8-openjdk-amd64"
export GRADLE_HOME=/opt/android-studio/gradle/gradle-4.1
export PATH=\$JAVA_HOME/bin:\$ANDROID_HOME/tools:\$ANDROID_HOME/platform-tools:\$GRADLE_HOME/bin:\$ANDROID_HOME/emulator/:\$PATH

EOT


# Snap install extras
# sudo snap install remmina
# sudo snap install dbeaver-ce --edge
# sudo snap install slack --classic
# sudo snap install eclipse --classic
# sudo snap install skype --classic
# sudo snap install gimp
# sudo snap install ghex-udt
# sudo snap install easy-disk-cleaner
# sudo snap install pycharm-community --classic
# sudo snap install intellij-idea-community --classic

# Android Studio
# wget https://gitlab.com/jsolemos/scripts-util/raw/master/bootstrap-enviroment/programas-e-ferramentas/android-studio.sh
# chmod +x android-studio.sh
# bash ./android-studio.sh



