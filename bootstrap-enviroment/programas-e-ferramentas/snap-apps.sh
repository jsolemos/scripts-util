# Snap install
sudo snap install code --classic
sudo snap install google-cloud-sdk --classic
sudo snap install kubectl --classic
sudo snap install insomnia
sudo snap install aws-cli --classic
sudo snap install eclipse --classic
sudo snap install slack --classic
sudo snap install skype --classic
sudo snap install remmina
sudo snap install dbeaver-ce --edge
# Snap install extras
sudo snap install gimp
sudo snap install ghex-udt
sudo snap install easy-disk-cleaner
sudo snap install pycharm-community --classic
sudo snap install intellij-idea-community --classic
