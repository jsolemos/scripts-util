### kube-pods

Busca e Mostra pods de um namespace

    $ ./kubernetes/kube-pods namespace pod-part-name(opcional)

### kube-logs

Busca e Mostra logs de um pod por parte do nome do pod e namespace

    $ ./kubernetes/kube-logs pod-part-name namespace

### kube-find-pod

Busca um pode a partir de parte de seu nome e namespace

    $ kube-find-pod pod-part-name namespace

### kube-port-forward

Redireciona porta de um pod para uma porta local

    $ kube-port-forward pod-part-name namespace port-host port-pod
