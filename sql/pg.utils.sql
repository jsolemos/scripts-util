-- CONSULTA DE PROCESSOS EM EXECUÇÃO 
SELECT
  pid,
  now() - pg_stat_activity.query_start AS duration,
  query,
  state,
  usename,
  client_addr, 
  query
FROM pg_stat_activity
WHERE (now() - pg_stat_activity.query_start) > interval '1 minutes';


-- FINALIZA PROCESSOS
SELECT pg_terminate_backend(pid)